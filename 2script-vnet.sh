# Creation du Vnet #

az network vnet create \
    --name Vnet \
    --resource-group OCC_ASD_Mickael \
    --network-security-group GsMika \
    --address-prefix 10.0.5.0/24 \
    --subnet-name Snet \
    --subnet-prefix 10.0.5.0/29
