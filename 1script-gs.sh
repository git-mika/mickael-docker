# creation du groupe de securité

az network nsg create --name GsMika \
                      --resource-group OCC_ASD_Mickael

# creation de la regle ssh

az network nsg rule create --name ssh \
                           --nsg-name GsMika \
                           --priority 100 \
                           --resource-group OCC_ASD_Mickael \
                           --destination-port-ranges 22 \
                           --source-address-prefixes 92.175.74.250

az network nsg rule create --name http \
                           --nsg-name GsMika \
                           --priority 110 \
                           --resource-group OCC_ASD_Mickael \
                           --destination-port-ranges 80 \
                           --source-address-prefixes 0.0.0.0
