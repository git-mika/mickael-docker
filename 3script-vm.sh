# Variables #
Resource_group_name="OCC_ASD_Mickael"
VmName="docker_master"
Image="Debian11"
UserName="mika"
ip_address="10.0.5.10"
netmask="255.255.255.0"
security_group_name="GsMika"

# Creation de la VM #
az vm create \
    --resource-group $Resource_group_name \
    --vnet-name Vnet \
    --subnet Snet \
    --name $VmName \
    --image $Image \
    --size Standard_B2s \
    --admin-username $UserName \
    --generate-ssh-keys

# pause #
sleep 15

# Creation de la carte reseau #
az network nic create \
    --resource-group-name $Resource_group_name \
    --name Nic \
    --private-ip-address-version IPv4 \
    --vnet-name Vnet \
    --subnet Snet

# pause #
sleep 15 

# Ajouter la carte reseau a la VM #
az vm nic add \
    --nics Nic \
    --resource-group $Resource_group_name \
    --vm-name docker_master \

# pause #
sleep 15

# Ajout au groupe de securite #
az network nic update \
    --resource-group $Resource_group_name \
    --name Nic \
    --network-security-group GsMika

# pause # 
sleep 15 

# Stocker l’adresse IP pour une connexion SSH #
export IP_ADDRESS=$(az vm show --show-details --resource-group $Resource_group_name --name $VmName --query publicIps --output tsv)


# Variable SSH #
SshConnexion="ssh -o StrictHostKeyChecking=no $UserName@$IP_ADDRESS"


# Connection SSH a la machine #
$SshConnexion
piou piou 