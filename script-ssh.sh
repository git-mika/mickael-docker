#!/bin/bash

# Installer le serveur SSH
sudo apt-get update
sudo apt-get install -y openssh-server

# Autoriser l'accès SSH pour l'utilisateur 'adminUsername' (remplacez-le par votre nom d'utilisateur)
sudo sh -c "echo 'mika ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers"
sudo systemctl restart sshd
