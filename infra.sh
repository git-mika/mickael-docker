#!/bin/bash

# Exécuter le premier script
chmod 755 1script-gs.sh
./1script-gs.sh

sleep 10

# Exécuter le deuxième script
chmod 755 2script-vnet.sh
./2script-vnet.sh

sleep 10

# Exécuter le troisième script
chmod 755 3script-vm.sh
./3script-vm.sh
